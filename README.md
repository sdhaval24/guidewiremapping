### Mapping Validation Automation ###

#### Quick summary
* This is a simple java application that quickly generates CSV file from JSON file to provide SAPI -> GW Mappings
#### Summary of set up
* Clone the project and open in you favorite choice of IDE.
#### How to run application
* mvn clean
* mvn install
* copy files and paste inside auto/home/ref Folder inside resources
* Run application
* Corresponding CSV file be created inside respective Folder